package main

import (
	"fmt"
	"gitlab.com/MatyushkinM/ci-golang-demo-matyushkin/hello"
)

func main() {
	fmt.Println(hello.Greet())
}